from django.urls import path
from . import views

urlpatterns = [
    path('upload/', views.process_form, name='upload'),
    path('success/', views.success, name='success'),
]
