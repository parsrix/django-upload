from django.db.models import FileField
from django.forms import forms
from django.template.defaultfilters import filesizeformat
from django.conf import settings
from django.core.files.images import get_image_dimensions
from django.core.files.uploadedfile import InMemoryUploadedFile
import os
import re
import magic


def dynamically_get_upload_dir(instance, filename):
    """" Dynamically get upload directory according to settings fields """
    return os.path.join(settings.UPLOAD_POLICY[instance.subject_type]['dir'], filename)


def dynamically_get_upload_max_size(subject_type):
    """" Dynamically get upload max size according to settings fields """
    return settings.UPLOAD_POLICY[subject_type]['max_upload_size']


def dynamically_get_upload_content_types(subject_type):
    """" Dynamically get upload content types according to settings fields """
    return settings.UPLOAD_POLICY[subject_type]['content_type_list']


def dynamically_get_mime_type(file):
    """ Dynamically get mime type of in memory uploaded file or temporary file"""
    if file.__class__ == InMemoryUploadedFile:
        return magic.from_buffer(file.file.getvalue(), mime=True)
    else:
        return magic.from_buffer(file.file.read(), mime=True)


class ContentTypeRestrictedFileField(FileField):
    def clean(self, *args, **kwargs):
        data = super(ContentTypeRestrictedFileField, self).clean(*args, **kwargs)
        try:
            content_type = data.file.content_type
            mime_type = dynamically_get_mime_type(data.file)
            if content_type != mime_type:
                raise forms.ValidationError('این نوع فایل پشتیبانی نمی شود')
            subject_type = data.instance.subject_type
            try:
                allowed_mime_types = dynamically_get_upload_content_types(subject_type)
                allowed_upload_max_size = dynamically_get_upload_max_size(subject_type)
            except Exception as e:
                # TODO: add exception into logger
                print(type(e))  # the exception instance
                print(e.args)  # arguments stored in .args
                print(e)
                raise forms.ValidationError('مشکلی در پردازش فایل وجود دارد. با پشتیبانی در تماس باشید')
            if mime_type in allowed_mime_types:
                if re.match(r'^image/(.*)', mime_type, re.I):
                    width, height = get_image_dimensions(data.file)
                    if width < settings.UPLOAD_POLICY[subject_type]['img_min_width'] or height < \
                            settings.UPLOAD_POLICY[subject_type]['img_min_height']:
                        raise forms.ValidationError('عرض و ارتفاع تصویر ارسالی نامعتبر می باشد')
                if data.file.size > settings.UPLOAD_POLICY[subject_type]['max_upload_size']:
                    raise forms.ValidationError(
                        'حجم تصویر باید کمتر یا مساوی {0} باشد'.format(filesizeformat(allowed_upload_max_size)))
            else:
                raise forms.ValidationError('این نوع فایل پشتیبانی نمی شود')
        except AttributeError as e:
            # TODO: add exception into logger
            print(type(e))  # the exception instance
            print(e.args)  # arguments stored in .args
            print(e)
            raise forms.ValidationError('مشکلی در پردازش فایل وجود دارد. با پشتیبانی در تماس باشید')
        return data
