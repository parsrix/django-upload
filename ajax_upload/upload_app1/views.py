from django.shortcuts import render, redirect
from .forms import MediaInfoForm


def process_form(request):
    try:
        if request.method == 'POST':
            form = MediaInfoForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
                return redirect('/app1/success/')
        else:
            form = MediaInfoForm()
        return render(request, 'upload.html', {'form': form})
    except Exception as e:
        raise Exception(e)


def success(request):
    return render(request, 'success.html')
