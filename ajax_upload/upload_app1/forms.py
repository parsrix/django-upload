from django import forms
from .models import MediaInfo


class MediaInfoForm(forms.ModelForm):
    class Meta:
        model = MediaInfo
        fields = {'user', 'file', 'subject_type', 'is_available'}
