from django.db import models
from django.contrib.auth.models import User
from .utils import ContentTypeRestrictedFileField, dynamically_get_upload_dir
from django.conf import settings


class MediaInfo(models.Model):
    CONTENT_TYPE_CHOICES = (
        (1, settings.UPLOAD_POLICY[1]['title']),
        (2, settings.UPLOAD_POLICY[2]['title']),
        (3, settings.UPLOAD_POLICY[3]['title']),
        (4, settings.UPLOAD_POLICY[4]['title']),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    file = ContentTypeRestrictedFileField(upload_to=dynamically_get_upload_dir)
    subject_type = models.IntegerField(choices=CONTENT_TYPE_CHOICES)
    created = models.DateTimeField('created', auto_now_add=True)
    modified = models.DateTimeField('modified', auto_now=True)

    # TODO: consider True value in corresponding response view
    is_available = models.BooleanField(default=False)
